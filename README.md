# sight-data

This repository contains additional data need to be download to run all the unit tests in [sight](https://git.ircad.fr/sight/sight).

You can found how to use this repository in the [testing documentation](https://sight.pages.ircad.fr/sight-doc/Testing/index.html) section of the sight documentation.

### Prerequisites

To clone this repository, you'll need `git-lfs` installed on your computer. You can find instructions for your platform [there](https://git-lfs.github.com/).

### Warning

Please check that all data are really cloned. If `git-lfs` is not installed on your computer, the large files will be empty.
